# -*- coding: utf-8 -*-
import re
import json
import scrapy
import dateparser
from bs4 import BeautifulSoup
from datetime import datetime
from scrapy.exceptions import CloseSpider
from scrapy.shell import inspect_response
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode, quote_plus


class FriendSpider(scrapy.Spider):
    name = 'friend'
    allowed_domains = ['knowndonorregistry.com']
    login_url = 'https://knowndonorregistry.com/'
    profile_url = 'https://knowndonorregistry.com/mykdr/profile/{id}'
    friend_url = 'https://knowndonorregistry.com/index.php?option=com_easysocial&lang=&Itemid={itemid}'

    custom_settings = {
        'DOWNLOAD_DELAY': 1,
        'ITEM_PIPELINES': {},
    }


    def start_requests(self):
        self.date = datetime.now().isoformat()

        self.login = self.settings.get('LOGIN')
        self.password = self.settings.get('PASSWORD')

        self.headers = self.settings.get('DEFAULT_REQUEST_HEADERS')
        self.headers['User-Agent'] = self.settings.get('USER_AGENT')

        self.headers['Accept'] = 'application/json, text/javascript, */*; q=0.01'
        self.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
        self.headers['Origin'] = 'https://knowndonorregistry.com'
        self.headers['Sec-Fetch-Mode'] = 'cors'
        self.headers['Sec-Fetch-Site'] = 'same-origin'
        self.headers['X-Requested-With'] = 'XMLHttpRequest'

        self.body = 'tmpl=component&format=ajax&no_html=1&id={id}&option=com_easysocial&namespace=site%2Fcontrollers%2Ffriends%2Frequest&{token}=1'

        self.db = set()

        self.db_file = self.settings.get('DB_FILE')
        with open(self.db_file) as fin:
            for line in fin:
                self.db.add(line.strip())

        self.friends = []

        self.friends_file = self.settings.get('FRIENDS_FILE')
        with open(self.friends_file) as fin:
            for line in fin:
                self.friends.append(line.strip())

        yield scrapy.Request(self.login_url, callback=self.do_login, dont_filter=True)


    def do_login(self, response):

        formdata = {
                    'username': self.login,
                    'password': self.password,
                }

        r = scrapy.FormRequest.from_response(response, formdata=formdata, callback=self.check_login)

        return r


    def check_login(self, response):
        # inspect_response(response, self)

        if response.css('form.logout-form').extract_first():
            return self.after_login(response)
        else:
            return self.login_fail(response)


    def login_fail(self, response):
        # self.logger.critical("Unable to login")
        raise CloseSpider("Something wrong. Unable to login.")


    def after_login(self, response):
        return scrapy.Request(self.login_url, callback=self.add_friends, dont_filter=True)


    def add_friends(self, response):
        # inspect_response(response, self)

        for _id in self.friends:
            url = self.profile_url.format(id=_id)
            meta = {'id': _id}
            yield scrapy.Request(url, meta=meta, callback=self.add_friend)


    def add_friend(self, response):
        # inspect_response(response, self)

        if response.xpath('//div[contains(@class, "o-btn-group--es-friends")]/a[text()[contains(., "Add As Friend")]]').extract_first():
            try:
                scripts = ' '.join(response.css('script ::text').extract())

                match = re.search('window.ea\s*=\s*({.*?});', scripts, re.DOTALL)
                data = json.loads(match.group(1))

                token = data['token']
                ajax_url = data['ajaxUrl']
                itemid = re.search('.*?Itemid=(\d+)', data['ajaxUrl']).group(1)
            except:
                self.logger.warning("Unable to find token: {}".format(response.url))
            else:
                url = self.friend_url.format(itemid=itemid)
                body = self.body.format(id=response.meta['id'], token=token)
                r = scrapy.Request(url, method='POST', headers=self.headers, body=body, callback=self.parse_response, dont_filter=True)
                return r


    def parse_response(self, response):
        # inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except ValueError as e:
            self.logger.warning("Unable to parse response json: {}".format(e))

        return data

######################################################################
# Helpers
######################################################################


def _strip(string, default=None):
    if string:
        return string.strip()
    return default


def add_domain(domain, url):
    if not url.startswith('http'):
        return domain + url
    else:
        return url
