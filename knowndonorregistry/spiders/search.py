# -*- coding: utf-8 -*-
import re
import json
import scrapy
import dateparser
from bs4 import BeautifulSoup
from datetime import datetime
from scrapy.exceptions import CloseSpider
from scrapy.shell import inspect_response
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode, quote


class SearchSpider(scrapy.Spider):
    name = 'search'
    allowed_domains = ['knowndonorregistry.com']
    login_url = 'https://knowndonorregistry.com/'
    search_url = 'https://knowndonorregistry.com/advance-search/advanced/user'


    def start_requests(self):
        self.date = datetime.now().isoformat()

        self.login = self.settings.get('LOGIN')
        self.password = self.settings.get('PASSWORD')

        self.headers = self.settings.get('DEFAULT_REQUEST_HEADERS')
        self.headers['User-Agent'] = self.settings.get('USER_AGENT')

        self.headers['Accept'] = 'application/json, text/javascript, */*; q=0.01'
        self.headers['Content-Type'] = 'application/x-www-form-urlencoded'
        self.headers['Origin'] = 'https://knowndonorregistry.com'
        self.headers['Referer'] = 'https://knowndonorregistry.com/advance-search/advanced/user'
        self.headers['Sec-Fetch-Mode'] = 'cors'
        self.headers['Sec-Fetch-Site'] = 'same-origin'
        self.headers['X-Requested-With'] = 'XMLHttpRequest'

        if hasattr(self, 'from_date') and self.from_date:
            self.from_date = dateparser.parse(self.from_date, date_formats=['%Y-%m-%d']).date().strftime("%d-%m-%Y")
            self.body = 'tmpl=component&format=ajax&no_html=1&data=%7B%22criterias%5B%5D%22%3A%5B%22GENDER%7Cgender%22%2C%22JOOMLA_LASTLOGIN%7Cjoomla_lastlogin%22%5D%2C%22datakeys%5B%5D%22%3A%5B%22%22%2C%22%22%5D%2C%22operators%5B%5D%22%3A%5B%22equal%22%2C%22greaterequal%22%5D%2C%22conditions%5B%5D%22%3A%5B%222%22%2C%22' + self.from_date + '%22%5D%2C%22matchType%22%3A%22all%22%2C%22sort%22%3A%22lastvisitDate%22%2C%22{token}%22%3A%221%22%2C%22Itemid%22%3A%22{itemid}%22%2C%22option%22%3A%22com_easysocial%22%2C%22view%22%3A%22search%22%2C%22layout%22%3A%22advanced%22%2C%22type%22%3A%22user%22%7D&nextlimit={nextlimit}&option=com_easysocial&namespace=site%2Fcontrollers%2Fsearch%2Floadmore&{token}=1'
        else:
            self.body = 'tmpl=component&format=ajax&no_html=1&data=%7B%22criterias%5B%5D%22%3A%22GENDER%7Cgender%22%2C%22datakeys%5B%5D%22%3A%22%22%2C%22operators%5B%5D%22%3A%22equal%22%2C%22conditions%5B%5D%22%3A%222%22%2C%22matchType%22%3A%22all%22%2C%22sort%22%3A%22lastvisitDate%22%2C%22{token}%22%3A%221%22%2C%22Itemid%22%3A%22{itemid}%22%2C%22option%22%3A%22com_easysocial%22%2C%22view%22%3A%22search%22%2C%22layout%22%3A%22advanced%22%2C%22type%22%3A%22user%22%7D&nextlimit={nextlimit}&option=com_easysocial&namespace=site%2Fcontrollers%2Fsearch%2Floadmore&{token}=1'

        self.db = set()

        self.db_file = self.settings.get('DB_FILE')
        with open(self.db_file) as fin:
            for line in fin:
                self.db.add(line.strip())

        yield scrapy.Request(self.login_url, callback=self.do_login, dont_filter=True)


    def do_login(self, response):

        formdata = {
                    'username': self.login,
                    'password': self.password,
                }

        r = scrapy.FormRequest.from_response(response, formdata=formdata, callback=self.check_login)

        return r


    def check_login(self, response):
        # inspect_response(response, self)

        if response.css('form.logout-form').extract_first():
            return self.after_login(response)
        else:
            return self.login_fail(response)


    def login_fail(self, response):
        # self.logger.critical("Unable to login")
        raise CloseSpider("Something wrong. Unable to login.")


    def after_login(self, response):
        return scrapy.Request(self.search_url, callback=self.do_search)


    def do_search(self, response):
        # inspect_response(response, self)

        scripts = ' '.join(response.css('script ::text').extract())

        try:
            match = re.search('window.ea\s*=\s*({.*?});', scripts, re.DOTALL)
            data = json.loads(match.group(1))

            self.nextlimit = 0
            self.token = data['token']
            self.ajax_url = data['ajaxUrl']
            self.itemid = re.search('.*?Itemid=(\d+)', data['ajaxUrl']).group(1)
        except:
            raise CloseSpider("Unable to find token.")

        body = self.body.format(token=self.token, itemid=self.itemid, nextlimit=self.nextlimit)
        r = scrapy.Request(self.ajax_url, method='POST', headers=self.headers, body=body, callback=self.parse_search, dont_filter=True)

        return r


    def parse_search(self, response):
        # inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except ValueError as e:
            raise CloseSpider("Unable to parse json: {}".format(e))

        html = data[0]['data'][0]
        nextlimit = data[0]['data'][1]

        soup = BeautifulSoup(html, 'lxml')
        for div in soup.find_all("div", {"class": "es-list-item__title"}):
            for url in div.find_all('a', href=True):
                u = url['href']
                i = re.search('profile/(\d+)-', u).group(1)

                if i not in self.db:
                    yield response.follow(u, callback=self.parse_profile)

        # self.logger.info("nextlimit: {}, bool: {}".format(nextlimit, not (nextlimit == '-1' or nextlimit is None or not html)))
        if not (nextlimit == '-1' or nextlimit is None or not html):
            self.nextlimit = nextlimit
            body = self.body.format(token=self.token, itemid=self.itemid, nextlimit=self.nextlimit)
            yield scrapy.Request(self.ajax_url, method='POST', headers=self.headers, body=body, callback=self.parse_search, dont_filter=True)


    def parse_profile(self, response):
        # inspect_response(response, self)

        d = {}

        d['scraping_date'] = self.date
        d['id'] = response.css('.es-profile-header ::attr(data-id)').extract_first()
        d['url'] = response.url

        d['name'] = _strip(' '.join(response.css('h1.es-profile-header__title a ::text').extract()))

        d['avatar_image_path'] = response.css('img[data-avatar-image] ::attr(src)').extract_first()

        if not d['avatar_image_path']:
            d['avatar_image_path'] = ''

        # if 'avatars/defaults/profiles' in d['avatar_image_path']:
        #     d['avatar_image_path'] = ''

        d['restricted'] = False
        d['details'] = {}

        if response.css('.es-restricted-message'):
            d['restricted'] = True
            yield d

        trs = response.css('div.es-content tr')
        for tr in trs:
            label = _strip(' '.join(filter(None, [s.strip().rstrip(':') for s in tr.css('.profile-data-label ::text').extract()])))
            value = _strip('\n'.join(filter(None, [s.strip() for s in tr.css('.profile-data-info ::text').extract()])))

            if label and value:
                d['details'][label] = value

        albums = response.xpath('//ul[contains(@class, "o-nav")]/li/a[text()[contains(.,"Album")]]/b/text()').extract_first()

        if albums != '0':
            url = response.url + '/albums'
            meta = {'item': d}
            yield response.follow(url, meta=meta, callback=self.parse_albums)
        else:
            yield d


    def parse_albums(self, response):
        d = response.meta['item']
        urls = response.css('.es-side-widget ul li a ::attr(href)').extract()

        if urls:
            url = urls.pop(0)
            meta = {'item': d, 'urls': urls}
            return response.follow(url, meta=meta, callback=self.add_images)
        else:
            return d


    def add_images(self, response):
        d = response.meta['item']
        urls = response.meta['urls']

        images = response.css('li[data-photo-original-button] a ::attr(href)').extract()

        if 'image_urls' not in d:
            d['image_urls'] = []

        d['image_urls'].extend(images)

        if urls:
            url = urls.pop(0)
            meta = {'item': response.meta['item'], 'urls': urls}
            return response.follow(url, meta=meta, callback=self.add_images)
        else:
            return d

######################################################################
# Helpers
######################################################################


def _strip(string, default=None):
    if string:
        return string.strip()
    return default


def add_domain(domain, url):
    if not url.startswith('http'):
        return domain + url
    else:
        return url
