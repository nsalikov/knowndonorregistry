# -*- coding: utf-8 -*-
import re
import json
import scrapy
import dateparser
from bs4 import BeautifulSoup
from datetime import datetime
from scrapy.exceptions import CloseSpider
from scrapy.shell import inspect_response
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode, quote_plus


class MessageSpider(scrapy.Spider):
    name = 'message'
    allowed_domains = ['knowndonorregistry.com']
    login_url = 'https://knowndonorregistry.com/'
    msg_url = 'https://knowndonorregistry.com/index.php'

    custom_settings = {
        'DOWNLOAD_DELAY': 1,
        'ITEM_PIPELINES': {},
    }


    def start_requests(self):
        self.date = datetime.now().isoformat()

        self.login = self.settings.get('LOGIN')
        self.password = self.settings.get('PASSWORD')

        self.headers = self.settings.get('DEFAULT_REQUEST_HEADERS')
        self.headers['User-Agent'] = self.settings.get('USER_AGENT')

        self.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
        self.headers['Origin'] = 'https://knowndonorregistry.com'
        self.headers['Sec-Fetch-Mode'] = 'cors'
        self.headers['Sec-Fetch-Site'] = 'same-origin'
        self.headers['X-Requested-With'] = 'XMLHttpRequest'

        self.body = 'conversekit=1&option=com_easysocial&namespace=create&format=json&limitstart=0&uid=&recipientId={recipientid}&message={msg}&{token}=1'

        self.db = set()

        self.db_file = self.settings.get('DB_FILE')
        with open(self.db_file) as fin:
            for line in fin:
                self.db.add(line.strip())

        self.msgs = []

        self.msgs_file = self.settings.get('MSGS_FILE')
        with open(self.msgs_file) as fin:
            for line in fin:
                d = json.loads(line)
                self.msgs.append(d)

        yield scrapy.Request(self.login_url, callback=self.do_login, dont_filter=True)


    def do_login(self, response):

        formdata = {
                    'username': self.login,
                    'password': self.password,
                }

        r = scrapy.FormRequest.from_response(response, formdata=formdata, callback=self.check_login)

        return r


    def check_login(self, response):
        # inspect_response(response, self)

        if response.css('form.logout-form').extract_first():
            return self.after_login(response)
        else:
            return self.login_fail(response)


    def login_fail(self, response):
        # self.logger.critical("Unable to login")
        raise CloseSpider("Something wrong. Unable to login.")


    def after_login(self, response):
        return scrapy.Request(self.login_url, callback=self.send_msg, dont_filter=True)


    def send_msg(self, response):
        # inspect_response(response, self)

        scripts = ' '.join(response.css('script ::text').extract())

        try:
            match = re.search('window.ea\s*=\s*({.*?});', scripts, re.DOTALL)
            data = json.loads(match.group(1))

            self.token = data['token']
        except:
            raise CloseSpider("Unable to find token.")

        for d in self.msgs:
            body = self.body.format(recipientid=d['recipientid'], msg=quote_plus(d['msg']), token=self.token)
            r = scrapy.Request(self.msg_url, method='POST', headers=self.headers, body=body, callback=self.parse_response, dont_filter=True)
            yield r


    def parse_response(self, response):
        # inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except ValueError as e:
            self.logger.warning("Unable to parse response json: {}".format(e))

        return data

######################################################################
# Helpers
######################################################################


def _strip(string, default=None):
    if string:
        return string.strip()
    return default


def add_domain(domain, url):
    if not url.startswith('http'):
        return domain + url
    else:
        return url
